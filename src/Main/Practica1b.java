package Main;

import QAP.*;
import Util.Print;
import Util.PrintToConsole;

import java.io.FileNotFoundException;

public class Practica1b {
    public static final int GRASP = 1;
    public static final int ILS = 2;
    public static final String[] PROBLEMAS = {"Els19", "Chr20a", "Chr25a", "Nug25", "Bur26a", "Bur26b", "Tai30a", "Tai30b", "Esc32a", "Kra32", "Tai35a", "Tai35b", "Tho40", "Tai40a", "Sko42", "Sko49", "Tai50a", "Tai50b", "Tai60a", "Lipa90a"};

    public static void main(String[] args){

        Print printer = new PrintToConsole();

        printer.printCabecera();

        int tipoSolucion = printer.menuTipoSolucion();
        int problemIndex = printer.menuProblemas()-1;
        int numIteration = 1;
        if(problemIndex != 21 && tipoSolucion != 3) numIteration = printer.menuIteraciones();
        long semilla = printer.menuSemilla();
        //semilla = 715225739;

        SolucionMultiarranque grasp;
        SolucionMultiarranque ils;
        String path;

        try {
            switch (tipoSolucion) {
                case Practica1b.GRASP:
                    if(problemIndex != PROBLEMAS.length) {

                        path = "data/" + PROBLEMAS[problemIndex].toLowerCase() + ".dat";
                        grasp = new Grasp(path, semilla);

                        ModelSolucion[] solucionGRASP = grasp.getNSoluciones(numIteration);
                        printer.printResultados(solucionGRASP, tipoSolucion, problemIndex);

                    } else {
                            for(int i = 0; i < PROBLEMAS.length; i++){
                                if(!PROBLEMAS[i].startsWith("Bur")) {

                                    path = "data/" + PROBLEMAS[i].toLowerCase() + ".dat";
                                    grasp = new Grasp(path, semilla);

                                    ModelSolucion[] solucionGRASP = grasp.getNSoluciones(numIteration);
                                    printer.printResultados(solucionGRASP, tipoSolucion, i);

                                }
                            }
                    }
                    break;
                case Practica1b.ILS:
                    if(problemIndex != PROBLEMAS.length) {

                        path = "data/" + PROBLEMAS[problemIndex].toLowerCase() + ".dat";

                        ils = new Ils(path, semilla);
                        ModelSolucion[] solucionILS = ils.getNSoluciones(numIteration);

                        printer.printResultados(solucionILS, tipoSolucion, problemIndex);

                    } else for (int i = 0; i < PROBLEMAS.length; i++)
                        if(!PROBLEMAS[i].startsWith("Bur")) {
                            path = "data/" + PROBLEMAS[i].toLowerCase() + ".dat";
                            ils = new Ils(path, semilla);

                            ModelSolucion[] solucionILS = ils.getNSoluciones(numIteration);
                            printer.printResultados(solucionILS, tipoSolucion, i);
                        }
                    break;
                case 3:
                    if(problemIndex != PROBLEMAS.length) {

                        path = "data/" + PROBLEMAS[problemIndex].toLowerCase() + ".dat";

                        grasp = new Grasp(path, semilla);
                        ils = new Ils(path, semilla);

                        ModelSolucion[] solucionGRASP = grasp.getNSoluciones(numIteration);
                        ModelSolucion[] solucionILS = ils.getNSoluciones(numIteration);

                        printer.printResultados(solucionGRASP, GRASP, problemIndex);
                        printer.printResultados(solucionILS, ILS, problemIndex);

                    } else
                        for (int i = 0; i < PROBLEMAS.length; i++) {
                            if(!PROBLEMAS[i].startsWith("Bur")) {

                                path = "data/" + PROBLEMAS[i].toLowerCase() + ".dat";

                                grasp = new Grasp(path, semilla);
                                ils = new Ils(path, semilla);

                                ModelSolucion[] solucionGRASP = grasp.getNSoluciones(1);
                                ModelSolucion[] solucionILS = ils.getNSoluciones(1);

                                printer.printResultados(solucionGRASP, GRASP, i);
                                printer.printResultados(solucionILS, ILS, i);

                            }

                    }
                    break;
            }

        } catch (FileNotFoundException exc) {
            printer.mostrarFileNotFoundError(exc);
        }

    }
}