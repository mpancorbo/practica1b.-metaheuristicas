package Util;

import Main.Practica1b;
import QAP.ModelSolucion;

import java.io.FileNotFoundException;
import java.util.Scanner;

public class PrintToConsole implements Print {

    public void printResultados(ModelSolucion[] soluciones, int tipoSolucion, int indexProblem) {
        String enfoque = getEnfoque(tipoSolucion);
        if(soluciones.length == 1) {
            System.out.println("Solución " + enfoque + " para el problema " + Practica1b.PROBLEMAS[indexProblem]);
            System.out.println("---------------------------------------");
            printResultado(soluciones[0]);
        }else {
            for (int i = 0; i < soluciones.length; i++){
                System.out.println("Iteración " + (i+1) + " " + enfoque + " " + Practica1b.PROBLEMAS[indexProblem]);
                printResultado(soluciones[i]);
            }

            printEstadisticas(soluciones, tipoSolucion, indexProblem);
        }
    }

    public void printResultado(ModelSolucion solucion){
        solucion.printSolucion();
        System.out.println("Coste: " + solucion.getCoste());
        System.out.println("Tiempo empleado: " + solucion.getTimeElapsed());
        System.out.println();
    }

    private void printEstadisticas(ModelSolucion[] soluciones, int tipo, int indexProblem){
        ModelSolucion mejor = soluciones[0];
        ModelSolucion peor = soluciones[0];

        for (ModelSolucion solucion : soluciones) {
            if (solucion.getCoste() > peor.getCoste())
                peor = solucion;
            else if (solucion.getCoste() < mejor.getCoste())
                mejor = solucion;
        }

        System.out.println("Estadísticas del problema " + Practica1b.PROBLEMAS[indexProblem] + " usando el enfoque " + getEnfoque(tipo));
        System.out.println("---------------------------------------------------------");

        System.out.println("Coste de la mejor solución: " + mejor.getCoste());
        System.out.println("Coste de la peor solución: " + peor.getCoste());

        double desviacion = Estadisticas.desviacionMedia(soluciones, mejor);
        double tiempoMedio = Estadisticas.tiempoMedio(soluciones);
        System.out.println("Desviación media: " + desviacion);
        System.out.println("Tiempo medio: " + tiempoMedio);
        System.out.println();
    }

    private String getEnfoque(int tipo){
        String enfoque;

        if (tipo == Practica1b.GRASP)
            enfoque = "GRASP";
        else
            enfoque = "ILS";

        return enfoque;
    }

    public int menuTipoSolucion(){
        Scanner in = new Scanner(System.in);
        int tipo;
        do {
            System.out.println("Escoge el método a usar para resolver el problema:");
            System.out.println("\t" + Practica1b.GRASP + "- GRASP");
            System.out.println("\t"+ Practica1b.ILS +"- ILS");
            System.out.println("\t"+ "3- Comparativa con todos los métodos");

            tipo = in.nextInt();
        }while(tipo < 1 || tipo > 3);

        return tipo;
    }

    public int menuProblemas(){
        Scanner in = new Scanner(System.in);
        int problemIndex;
        do{
            System.out.println("Escoge el problema que quieres resolver:");
            for(int i = 0; i < Practica1b.PROBLEMAS.length; i++){
                System.out.println("\t" + (i+1) + "- " + Practica1b.PROBLEMAS[i]);
            }
            System.out.println("\t" + (Practica1b.PROBLEMAS.length+1) + "- " + "Todos los problemas");
            problemIndex = in.nextInt();

        }while(problemIndex > (Practica1b.PROBLEMAS.length + 1) || problemIndex < 1);

        return problemIndex;
    }

    public int menuIteraciones(){
        Scanner in = new Scanner(System.in);
        int numIteration;
        System.out.println("Escoge el numero de iteraciones a realizar:");
        numIteration = in.nextInt();
        return numIteration;
    }

    public long menuSemilla(){
        Scanner in = new Scanner(System.in);
        long semilla;
        System.out.println("Escoge la semilla para la solución inicial:");
        semilla = in.nextLong();

        return semilla;
    }

    @Override
    public void mostrarFileNotFoundError(FileNotFoundException exc) {
        System.out.println("La ruta especificada para el archivo no existe: " + exc.getMessage());
    }

    @Override
    public void printCabecera() {
        System.out.println("Práctica 1.b. Metaheurísticas");
        System.out.println();
    }
}
