package Util;

import QAP.ModelSolucion;

/**
 * Created by mpancorbo on 07/10/14.
 */
public class Estadisticas {

    public static double desviacionMedia(ModelSolucion[] soluciones, ModelSolucion mejor){
        double desviacion = 0;
        float factor = 100 / (float)mejor.getCoste();

        for(int i = 0; i < soluciones.length; i++){
            desviacion += factor*(soluciones[i].getCoste() - mejor.getCoste());
        }
        desviacion /= soluciones.length;

        return desviacion;
    }

    public static double tiempoMedio(ModelSolucion[] soluciones){
        double tiempoMedio = 0;

        for(int i = 0; i < soluciones.length; i++){
            tiempoMedio += soluciones[i].getTimeElapsed();
        }

        tiempoMedio /= soluciones.length;

        return tiempoMedio;
    }
}
