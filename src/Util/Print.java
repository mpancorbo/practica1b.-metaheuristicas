package Util;

import QAP.ModelSolucion;

import java.io.FileNotFoundException;

public interface Print {

    public void printResultados(ModelSolucion[] soluciones, int tipoSolucion, int indexProblem);

    public int menuTipoSolucion();

    public int menuProblemas();

    public int menuIteraciones();

    public long menuSemilla();

    public void mostrarFileNotFoundError(FileNotFoundException exc);

    public void printCabecera();
}
