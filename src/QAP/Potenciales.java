package QAP;

/**
 * Created by mpancorbo on 24/10/14.
 */
public class Potenciales {

    public final static int FLUJOS = 0;
    public final static int DISTANCIAS = 1;

    private int[] vectorPotenciales;
    private int min;
    private int max;
    private int tipo;

    Potenciales(int[] vectorPotenciales, int max, int min, int tipo){
        this.vectorPotenciales = vectorPotenciales;
        this.min = min;
        this.max = max;
        this.tipo = tipo;
    }

    public void setVectorPotenciales(int[] vectorPotenciales) {
        this.vectorPotenciales = vectorPotenciales;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int[] getVectorPotenciales() {

        return vectorPotenciales;
    }

    public int getMin() {
        return min;
    }

    public int getMax() {
        return max;
    }

}
