package QAP;

import java.util.Random;

public class BusquedaLocal extends SolucionBasica {

    public BusquedaLocal(int tam, int[][] flujos, int[][] distancias, Random random){
        super(tam, flujos, distancias, random);
    }

    public ModelSolucion getSolucion(int[] solucion){

        int dontLookBits[] = new int[this.tam];

        int coste = funcionObjetivo(solucion);
        boolean mejora;
        do {
            mejora = false;
            for (int i = 0; i < this.tam; i++) {
                if (dontLookBits[i] == 0 && !mejora) {
                    for (int j = 0; j < this.tam; j++) {
                        if (i != j && !mejora) {
                            int diff = getDiffCoste(solucion, i, j);

                            if (diff < 0) {
                                mejora = true;
                                coste += diff;

                                int aux = solucion[i];
                                solucion[i] = solucion[j];
                                solucion[j] = aux;

                                dontLookBits[i] = 0;
                                dontLookBits[j] = 0;
                            }
                        }
                    }
                    if (!mejora) dontLookBits[i] = 1;
                }
            }
        }while(mejora);

        return new ModelSolucion(solucion, 0, coste);
    }

}
