package QAP;

import java.io.FileNotFoundException;

public abstract class SolucionMultiarranque extends SolucionBasica {

    public SolucionMultiarranque(String path, long semilla) throws FileNotFoundException {
        super(path, semilla);
    }

    public abstract ModelSolucion getSolucion();

    public ModelSolucion[] getNSoluciones(int n) throws FileNotFoundException{
        ModelSolucion[] soluciones = new ModelSolucion[n];
        for(int i = 0; i < n; i++)
            soluciones[i] = this.getSolucion();

        return soluciones;
    }
}
