package QAP;

import java.io.FileNotFoundException;
import java.util.Random;

public class Greedy extends SolucionBasica{

    public Greedy(int tam, int[][] flujos, int[][] distancias, Random random) throws FileNotFoundException {
        super(tam, flujos, distancias, random);
    }

    private static int[] generaPotenciales(int[][] matriz){
        int tam = matriz.length;
        int[] potenciales = new int[tam];

        for(int i = 0; i < tam; i++){
            for (int j = 0; j < tam; j++)
                potenciales[i] += matriz[i][j];
        }

        return potenciales;
    }

    public ModelSolucion getSolucion(){
        long timeStart = System.currentTimeMillis();

        int[] flujosPotenciales = generaPotenciales(this.flujos);
        int[] distanciasPotenciales = generaPotenciales(this.distancias);

        int[] solucion = new int[this.tam];

        int maxFlujo;
        int minDistancia;

        for(int i = 0; i < tam; i++){
            maxFlujo = getIndexOfMax(flujosPotenciales);
            minDistancia = getIndexOfMin(distanciasPotenciales);

            solucion[maxFlujo] = minDistancia;
            flujosPotenciales[maxFlujo] = Integer.MIN_VALUE;
            distanciasPotenciales[minDistancia] = Integer.MAX_VALUE;
        }

        return new ModelSolucion(solucion, System.currentTimeMillis() - timeStart, this.funcionObjetivo(solucion));
    }

    private static int getIndexOfMax(int[] vector){
        int max = 0;
        for(int i = 1; i < vector.length; i++)
            if(vector[i] > vector[max])
                max = i;

        return max;
    }
    private static int getIndexOfMin(int[] vector){
        int min = 0;
        for(int i = 1; i < vector.length; i++)
            if(vector[i] < vector[min])
                min = i;

        return min;
    }
}
