package QAP;

import java.io.FileNotFoundException;


public class Ils extends SolucionMultiarranque {

    private static final double SUBLIST_SIZE = 0.25;

    private BusquedaLocal busquedaLocal;
    private Greedy greedy;

    public Ils(String path, long semilla) throws FileNotFoundException {
        super(path, semilla);
        busquedaLocal = new BusquedaLocal(this.tam, this.flujos, this.distancias, this.random);
        greedy = new Greedy(this.tam, this.flujos, this.distancias, this.random);
    }

    @Override
    public ModelSolucion getSolucion(){

        long timeStart = System.currentTimeMillis();

        ModelSolucion mejorSolucion = greedy.getSolucion();

        mejorSolucion = busquedaLocal.getSolucion(mejorSolucion.getPermutacion());

        for(int i = 0; i < 24; i++){
            ModelSolucion nuevaSolucion = new ModelSolucion(null, 0, 0);

            nuevaSolucion.setPermutacion(mutaSolucion(mejorSolucion.getPermutacion()));
            nuevaSolucion.setCoste(this.funcionObjetivo(nuevaSolucion.getPermutacion()));


            nuevaSolucion = busquedaLocal.getSolucion(nuevaSolucion.getPermutacion());

            if (mejorSolucion.getCoste() > nuevaSolucion.getCoste())
                mejorSolucion = nuevaSolucion;

        }

        mejorSolucion.setTimeElapsed(System.currentTimeMillis() - timeStart);

        return mejorSolucion;
    }

    private int[] mutaSolucion(int[] solucion) {
        int tamSublist = (int) (this.tam * SUBLIST_SIZE);
        int inicio = this.random.nextInt(this.tam);
        int fin = (inicio + tamSublist);
        if(fin > (this.tam-1)){
            int [] solAux = new int[tamSublist];
            int cont = 0;
            fin = fin % this.tam;

            for(int i = inicio;i<this.tam;i++) {
                solAux[cont] = solucion[i];
                cont++;
            }
            for(int i = 0; i < fin; i++) {
                solAux[cont] = solucion[i];
                cont++;
            }

            for (int i = tamSublist-1; i > 0; i--){
                int randomIndex = this.random.nextInt(tamSublist);
                int aux = solAux[i];
                solAux[i] = solAux[randomIndex];
                solAux[randomIndex] = aux;
            }
            cont = 0;
            for(int i = 0; i < fin; i++)
                solucion[i] = solAux[cont++];
            for(int i = inicio; i < this.tam; i++)
                solucion[i] = solAux[cont++];
        } else {
            for (int i = fin; i > inicio; i--){
                int randomIndex = this.random.nextInt(tamSublist) + inicio;
                int aux = solucion[i];
                solucion[i] = solucion[randomIndex];
                solucion[randomIndex] = aux;
            }
        }

        return solucion;
    }

}
