package QAP;

import java.util.ArrayList;
import java.util.Random;

public class GreedyAleatorio extends SolucionBasica {

    private static final double ALFA = 0.3;

    ArrayList<Integer> localizacionesAsignadas;
    ArrayList<Integer> unidadesAsignadas;
    MatrizFactibles matrizFactibles;

    public GreedyAleatorio(int tam, int[][] flujos, int[][] distancias, Random random){
        super(tam, flujos, distancias, random);
    }

    public ModelSolucion getSolucion(){
        long timeStart = System.currentTimeMillis();

        localizacionesAsignadas = new ArrayList<Integer>();
        unidadesAsignadas = new ArrayList<Integer>();
        matrizFactibles = new MatrizFactibles(this.tam);

        int[] solucion = new int[this.tam];

        Potenciales flujosPotenciales = generaPotenciales(this.flujos, Potenciales.FLUJOS);
        Potenciales distanciasPotenciales = generaPotenciales(this.distancias, Potenciales.DISTANCIAS);

        ArrayList<Integer> flujosCandidatos = getCandidatosFaseInicial(flujosPotenciales, Potenciales.FLUJOS);
        ArrayList<Integer> distanciasCandidatas = getCandidatosFaseInicial(distanciasPotenciales, Potenciales.DISTANCIAS);

        for(int i = 0; i < 2; i++){
            int unidad;
            do {
                unidad = flujosCandidatos.get(this.random.nextInt(flujosCandidatos.size()));
            }while(unidadesAsignadas.contains(unidad));
            int localizacion;
            do {
                localizacion = distanciasCandidatas.get(this.random.nextInt(distanciasCandidatas.size()));
            }while(localizacionesAsignadas.contains(localizacion));
            solucion[unidad] = localizacion;
            unidadesAsignadas.add(unidad);
            localizacionesAsignadas.add(localizacion);
        }

        for(int i = 0 ; i < this.tam - 2; i++){
            updateCostes();
            ArrayList<Asignacion> listaCandidatos = getListaCandidatos(getUmbralCostes());
            Asignacion asignacion = listaCandidatos.get(this.random.nextInt(listaCandidatos.size()));

            unidadesAsignadas.add(asignacion.getUnidad());
            localizacionesAsignadas.add(asignacion.getLocalizacion());
            solucion[asignacion.getUnidad()] = asignacion.getLocalizacion();
        }

        return new ModelSolucion(solucion, System.currentTimeMillis() - timeStart, this.funcionObjetivo(solucion));
    }

    private Potenciales generaPotenciales(int[][] matriz, int tipo){
        int tam = matriz.length;
        int[] potenciales = new int[tam];

        int min = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;

        for(int i = 0; i < tam; i++){
            for (int j = 0; j < tam; j++)
                potenciales[i] += matriz[i][j] + matriz[j][i];

            if(potenciales[i] < min) min = potenciales[i];
            else if(potenciales[i] > max) max = potenciales[i];
        }

        return new Potenciales(potenciales, max, min, tipo);
    }

    private ArrayList<Integer> getCandidatosFaseInicial(Potenciales potenciales, int tipo){
        double umbral = getUmbralPotenciales(potenciales.getMin(), potenciales.getMax(), tipo);
        int[] vectorPotenciales = potenciales.getVectorPotenciales();

        if (tipo == Potenciales.FLUJOS)
            return getCandidatosFaseInicialFlujos(vectorPotenciales, umbral);
        else
            return getCandidatosFaseInicialDistancias(vectorPotenciales, umbral);
    }

    private ArrayList<Integer> getCandidatosFaseInicialFlujos(int[] potenciales, double umbral){
        ArrayList<Integer> candidatos = new ArrayList<Integer>();
        int indexMaxUnderUmbral = -1;
        int valueMaxUnderUmbral = Integer.MIN_VALUE;

        for(int i = 0; i < potenciales.length; i++){
            if(potenciales[i]  >= umbral)
                candidatos.add(i);
            else if(potenciales[i] >= valueMaxUnderUmbral) {
                indexMaxUnderUmbral = i;
                valueMaxUnderUmbral = potenciales[i];
            }
        }

        if(candidatos.size() == 1)
            candidatos.add(indexMaxUnderUmbral);

        return candidatos;
    }

    private ArrayList<Integer> getCandidatosFaseInicialDistancias(int[] potenciales, double umbral){
        ArrayList<Integer> candidatos = new ArrayList<Integer>();
        int indexMinAboveUmbral = -1;
        int valueMinAboveUmbral = Integer.MAX_VALUE;

        for(int i = 0; i < potenciales.length; i++){
            if(potenciales[i] <= umbral)
                candidatos.add(i);
            else if(potenciales[i] <= valueMinAboveUmbral) {
                indexMinAboveUmbral = i;
                valueMinAboveUmbral = potenciales[i];
            }
        }

        if(candidatos.size() == 1)
            candidatos.add(indexMinAboveUmbral);

        return candidatos;
    }

    private double getUmbralPotenciales(int min, int max, int tipo){
        if(tipo == Potenciales.FLUJOS)
            return max - ALFA * (max - min);

        return min + ALFA * (max - min);

    }

    private void updateCostes(){
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;

        int coste;
        for(int i = 0; i < this.tam; i++){
            if(!unidadesAsignadas.contains(i)) {
                for (int j = 0; j < this.tam; j++) {
                    if(!localizacionesAsignadas.contains(j)) {
                        updateCoste(i, j);
                        coste = matrizFactibles.getCostes(i,j);
                        if(coste < min) min = coste;
                        else if(coste > max) max = coste;
                    }
                }
            }
        }

        matrizFactibles.setMax(max);
        matrizFactibles.setMin(min);
    }

    private void updateCoste(int unidad, int localizacion){
        int coste = 0;
        for(int i = 0; i < localizacionesAsignadas.size(); i++)
            coste += this.flujos[unidad][unidadesAsignadas.get(i)] * this.distancias[localizacion][localizacionesAsignadas.get(i)];

        matrizFactibles.setCostes(unidad, localizacion, coste);
    }

    private double getUmbralCostes(){
        return matrizFactibles.getMin() + ALFA * (matrizFactibles.getMax() - matrizFactibles.getMin());
    }

    private ArrayList<Asignacion> getListaCandidatos(double umbral){
        ArrayList<Asignacion> listaCandidatos = new ArrayList<Asignacion>();

        for(int i = 0; i < this.tam; i++){
            if(!unidadesAsignadas.contains(i)){
                for(int j = 0; j < this.tam; j++){
                    if(!localizacionesAsignadas.contains(j)){
                        if(matrizFactibles.getCostes(i,j) <= umbral){
                            listaCandidatos.add(new Asignacion(i,j));
                        }
                    }
                }
            }
        }

        return listaCandidatos;
    }
}
