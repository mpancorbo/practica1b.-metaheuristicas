package QAP;

/**
 * Created by mpancorbo on 24/10/14.
 */
public class MatrizFactibles {
    private int[][] costes;
    private int max;
    private int min;

    MatrizFactibles(int tam){
        this.costes = new int[tam][tam];
    }

    public int getCostes(int unidad, int localizacion) {
        return costes[unidad][localizacion];
    }

    public void setCostes(int unidad, int localizacion, int coste) {
        this.costes[unidad][localizacion] = coste;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }
}
