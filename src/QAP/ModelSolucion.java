package QAP;

public class ModelSolucion {
    private long timeElapsed;
    private int[] permutacion;
    private int coste;

    public ModelSolucion(int[] _permutacion, long _timeElapsed, int _coste){
        this.coste = _coste;
        this.permutacion = _permutacion;
        this.timeElapsed = _timeElapsed;
    }

    public ModelSolucion(){}

    public long getTimeElapsed() {
        return timeElapsed;
    }

    public void setTimeElapsed(long timeElapsed) {
        this.timeElapsed = timeElapsed;
    }

    public int[] getPermutacion() {
        return permutacion.clone();
    }

    public void setPermutacion(int[] permutacion) {
        this.permutacion = permutacion;
    }

    public int getCoste() {
        return coste;
    }

    public void setCoste(int coste) {
        this.coste = coste;
    }

    public void printSolucion(){
        for (int aPermutacion : permutacion) System.out.print(aPermutacion + "\t");

        System.out.println();
    }
}
