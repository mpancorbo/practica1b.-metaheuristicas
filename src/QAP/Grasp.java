package QAP;

import java.io.FileNotFoundException;

public class Grasp extends SolucionMultiarranque {

    public Grasp(String path, long semilla) throws FileNotFoundException {
        super(path,semilla);
    }

    @Override
    public ModelSolucion getSolucion(){

        long timeStart = System.currentTimeMillis();

        GreedyAleatorio greedy = new GreedyAleatorio(this.tam, this.flujos, this.distancias, this.random);
        BusquedaLocal blocal = new BusquedaLocal(this.tam, this.flujos, this.distancias, this.random);

        ModelSolucion mejorSolucion = new ModelSolucion(null, 0, Integer.MAX_VALUE);

        for(int i = 0; i < 25; i++){
            ModelSolucion solucion = greedy.getSolucion();
            solucion = blocal.getSolucion(solucion.getPermutacion());

            if(solucion.getCoste() < mejorSolucion.getCoste())
                mejorSolucion = solucion;
        }

        return new ModelSolucion(mejorSolucion.getPermutacion(), System.currentTimeMillis() - timeStart, mejorSolucion.getCoste());
    }

}
