package QAP;

/**
 * Created by mpancorbo on 24/10/14.
 */
public class Asignacion {
    private int unidad;
    private int localizacion;

    Asignacion(int unidad, int localizacion){
        this.unidad = unidad;
        this.localizacion = localizacion;
    }

    public int getUnidad() {
        return unidad;
    }

    public int getLocalizacion() {
        return localizacion;
    }
}
