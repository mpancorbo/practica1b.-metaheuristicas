# README #

Práctica 1b para la asignatura Metaheurísticas, correspondiente al tercer curso del Grado en Ingeniería Informática de la Universidad de Jaén.

### ¿En qué consiste esta práctica? ###

El objetivo de la práctica es el de resolver problemas QAP (Quadratic Assignment Problem) usando soluciones metaheurísticas basadas en trayectorias múltiples, que si bien no tienen el objetivo de encontrar la solución óptima, optan por encontrar una solución razonablemente buena en un tiempo pequeño.

* Solución basada en el algoritmo GRASP
* Solución basada en el algoritmo ILS

### Estado actual ###

* Solución basada en GRASP
* Solución basada en ILS.

### Alumnos ###

* Manuel Pancorbo Pestaña (a.k.a. mpp00017)
* Manuel José Castro Damas (a.k.a. Xuela08)